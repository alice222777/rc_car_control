# rc_car_control

this is a simple RC car control code that works with :
- Arduino Mega 2560
- FrSky TFR6 receiver
- Futaba T12FGH 12 channel transmitter
- LM298N motor driver
